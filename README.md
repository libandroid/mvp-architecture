# Mvp Architecture

A Model-View-Presenter library for Android apps. Written in kotlin.

Current version: `0.10`

```groovy
implementation 'pl.bms:mvparchitecture:0.10'
```
minSdkVersion **15**
targetSdkVersion **27**

### MvpViewState
Every MvpView (Activity, Fragment or Custom View) has his own state, described by class that implements MvpViewState. 
ViewState should have all properties, that make it possible to describe the whole view. For example:
```kotlin
@Parcelize
data class SearcherViewState(
        val isLoading: Boolean,
        val searchResult: List<SearchResult>,
        val errorMessage: String?
) : MvpViewState
```
----------

So  MvpView should refresh whole UI by implementing one method:
```kotlin
 fun render(newState: SearcherViewState)
```
----------

You need implement Parcelable, because this library handle saving and restoring ViewState. (In custom views, there are used Serializable instead of Parcelable for technical reasons). 
Whole MvpViewState interface:
```kotlin
interface MvpViewState: Parcelable, Serializable
```

### MvpView

There are 3 abstract classes implementing the MvpView interface: **BaseMvpActivity**, **BaseMvpFragment**,  **BaseMvpFrameLayout**

You must implemet **render** method. For example:
```kotlin
override fun render(newState: SearchViewState) {
	progressBar.visibility = if (newStateisVisible) View.VISIBLE else View.INVISIBLE
	if (currentState?.searchResult != newState.searchResult) {
	    listAdapter.items = newState.searchResult.items
	}
	errorTextView.text = newState.errorMessage
}
```
You have access to **currentState** which is optional (it is automatically refreshed after every render call)

### 'BaseMvpActivity' 
You must set **layoutId** which is a id of layout in resources. For example:
```kotlin
override val layoutId = R.layout.activity_search
```
----------

Methods: `setupViews()` and `initActions()` are called from `onCreate()` method in **BaseMvpActivity** after creating presenter (Presenter is attached in `onResume()` method) and setting layout.
*They are optional, and you don't need to use them (They are empty in super class)*

----------

In BaseMvpActivity you can also set flag `statefulPresenter = true` to make the presenter retains after orientation change. 
This is available only in BaseMvpActivity *(not in BaseMvpFragment or BaseMvpFrameLayout)*
It works with `onRetainCustomNonConfigurationInstance()` [link](https://developer.android.com/reference/android/support/v4/app/FragmentActivity.html#onRetainCustomNonConfigurationInstance%28%29)
Default is false.


### 'BaseMvpFragment' 
You must set **layoutId** which is a id of layout in resources
```kotlin
override val layoutId = R.layout.activity_search
```
----------

Methods: `setupViews(view: View)` and `initActions(view: View)` are called from `onCreateView()` method in **BaseMvpFragment** after creating presenter (Presenter is attached in `onResume()` method) and inflating layout.
*They are optional, and you don�t need to use them (They are empty in super class)*

### 'BaseMvpFrameLayout' 

Method: `initView(context: Context, attrs: AttributeSet?, defStyleAttr: Int?)` is called from constructor, and it is place to create view.
After initView `initActions()` is called, where you should set listeners

### Create Presenter, saving and restoring states

in **BaseMvpActivity, BaseMvpFragment, BaseMvpFrameLayout** you must implement `createPresenter()` method. For example:
```kotlin
override fun createPresenter(restoredViewState: SearchViewState?): SearchContract.Presenter {
    return SearchPresenter(SearchModel(app.restManager, app.apiProvider), restoredViewState)
}
```
Saving and restoring state are handled in Base classes. You need only set presenter's currentState on initialization. For example:
```kotlin
override var currentState: SearchViewState = restoredViewState ?: SearchViewState(false, null, null)
```
When the presenter is attached, it always call refresh View State on View. So when presenter is created, MvpView is automatically refreshed with default or restored ViewState.

### Presenter's life cycle

**In BaseMvpActivity:**
Created -> in `onCreate()` 
Attached -> in `onResume()`
Detached -> in `onPause()`
Destroy -> in `onDestroy()`

**In BaseMvpFragment:**
Created -> in `onCreateView()` 
Attached -> in `onResume()`
Detached -> in `onPause()`
Destroy -> in `onDestroy()`

**In BaseMvpFrameLayout:**
Created -> in `onAttachedToWindow()`    *(or just before, in onRestoreInstanceState(), when state was saved in onSaveInstanceState())*
Attached -> in `onAttachedToWindow()` just after created
Detached -> in `onDetachedFromWindow()`
Destroy -> in `onDetachedFromWindow()` just after detached

### Presenter refreshing state:

When view is attached, presenter automatically refresh his ViewState.

If you want refresh it yourself, you should use presenter's method 
**refreshState(newState)** instead of ~~mvpView?.refreshState(newState)~~ Because in presenter's refreshState there is checked is newState isn't equals to currentState, and if is not, currentState is updated, and after that  view's refreshState is called.



### Used dependecies

	"pl.bms:navigation:$navigation_version"
	"org.jetbrains.kotlin:kotlin-stdlib-jre7:$kotlin_version"
    "com.android.support:appcompat-v7:$support_version"
    "io.reactivex.rxjava2:rxjava:$rx_version"
    "io.reactivex.rxjava2:rxandroid:$rxandroid_version"

## Dependecies verions 

```groovy
	navigation_version = '0.5'
	kotlin_version = '1.2.41'
    support_version = '27.1.1'
    rx_version = '2.1.8'
    rxandroid_version = '2.0.1'
```

## License
	Copyright 2018 BM Solution & Soft-AB

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.

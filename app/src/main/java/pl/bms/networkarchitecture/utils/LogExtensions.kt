@file:Suppress("unused")

package pl.bms.networkarchitecture.utils

import android.util.Log

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 10.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */
fun Any.logD(message: String?, throwable: Throwable? = null) {
    val tag = this::class.java.simpleName
    Log.d(tag, message, throwable)
}

fun Any.logV(message: String?, throwable: Throwable? = null) {
    val tag = this::class.java.simpleName
    Log.v(tag, message, throwable)
}

fun Any.logI(message: String?, throwable: Throwable? = null) {
    val tag = this::class.java.simpleName
    Log.i(tag, message, throwable)
}

fun Any.logW(message: String?, throwable: Throwable? = null) {
    val tag = this::class.java.simpleName
    Log.w(tag, message, throwable)
}

fun Any.logE(message: String?, throwable: Throwable? = null) {
    val tag = this::class.java.simpleName
    Log.e(tag, message, throwable)
}

fun Any.logWTF(message: String?, throwable: Throwable? = null) {
    val tag = this::class.java.simpleName
    Log.wtf(tag, message, throwable)
}
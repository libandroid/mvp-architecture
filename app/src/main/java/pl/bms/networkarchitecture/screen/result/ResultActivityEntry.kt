package pl.bms.networkarchitecture.screen.result

import android.content.Intent
import pl.bms.navigation.ActivityEntry

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 19.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */

class ResultActivityEntry(
        val firstNumber: Int,
        val secondNumber: Int
): ActivityEntry {

    override val type = ResultActivity::class.java

    override fun putToIntent(): Intent.() -> Unit = {
        putExtra(PARAM_FIRST_NUMBER, firstNumber)
        putExtra(PARAM_SECOND_NUMBER, secondNumber)
    }

    constructor(intent: Intent) : this(
            intent.getIntExtra(PARAM_FIRST_NUMBER, -1),
            intent.getIntExtra(PARAM_SECOND_NUMBER, -1)
    )

    companion object {
        const val PARAM_FIRST_NUMBER = "param.first.number"
        const val PARAM_SECOND_NUMBER = "param.second.number"
    }
}
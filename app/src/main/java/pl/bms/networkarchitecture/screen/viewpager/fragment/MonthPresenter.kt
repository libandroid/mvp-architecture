package pl.bms.networkarchitecture.screen.viewpager.fragment

import pl.bms.mvparchitecture.mvp.BaseMvpPresenter
import pl.bms.networkarchitecture.utils.elseDo
import pl.bms.networkarchitecture.utils.ifNotNull
import io.reactivex.disposables.Disposable
import java.util.*

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 16.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */

class MonthPresenter(private val model: MonthContract.Model, private val dateFrom: Date, private val dateTo: Date, restoredViewState: MonthViewState?)
    : BaseMvpPresenter<MonthViewState, MonthContract.View>(), MonthContract.Presenter {

    override var currentState: MonthViewState = restoredViewState ?: MonthViewState()

    private var disposable: Disposable? = null

    override fun onCreate() {
        refreshState(currentState.copy(isLoading = true))
        disposable = model.searchRepositoriesByDate(dateFrom, dateTo)
                .subscribe(
                        { result ->
                            ifNotNull(result.body()) { searchResult ->
                                refreshState(MonthViewState(searchResult = searchResult))
                            }.elseDo {
                                refreshState(MonthViewState(errorMessage = "Cannot load items\n${result.message()}"))
                            }
                        },
                        { error ->
                            refreshState(MonthViewState(errorMessage = "Cannot load items\n${error.message}"))
                        })
    }

    override fun onDestroy() {
        disposable?.dispose()
    }
}
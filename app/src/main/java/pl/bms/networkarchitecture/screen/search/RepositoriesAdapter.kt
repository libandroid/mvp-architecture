package pl.bms.networkarchitecture.screen.search

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.repo_viewholder.view.*
import pl.bms.networkarchitecture.R
import pl.bms.networkarchitecture.model.GitHubRepo


/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 03.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */


class RepositoriesAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var items = emptyList<GitHubRepo>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.repo_viewholder, parent, false)
        return RepoViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as RepoViewHolder).apply {
            nameTv.text = items[position].name
            dateTv.text = items[position].createdAt.toString()
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class RepoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameTv: TextView = itemView.repoViewHolderNameTV
        val dateTv: TextView = itemView.repoViewHolderDateTV
    }

}
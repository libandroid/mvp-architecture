package pl.bms.networkarchitecture.screen.search

import pl.bms.mvparchitecture.mvp.BaseMvpPresenter
import pl.bms.networkarchitecture.utils.logD
import pl.bms.networkarchitecture.utils.logE
import pl.bms.networkarchitecture.utils.logW

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 10.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */

class SearchPresenter(val model: SearchContract.Model, restoredViewState: SearchViewState?): BaseMvpPresenter<SearchViewState, SearchContract.View>(), SearchContract.Presenter {

    override var currentState: SearchViewState = restoredViewState ?: SearchViewState(false, null)

    override fun search(searchString: String) {
        refreshState(currentState.copy(isLoading = true))
        model.search(searchString)
                .subscribe(
                        { searchResult ->
                            logD("network result is successful - ${searchResult.isSuccessful}")
                            refreshState(currentState.copy(isLoading = false))
                            searchResult.body()?.also { result ->
                                refreshState(currentState.copy(searchResult = result))
                            }
                            searchResult.errorBody()?.also {
                                logW("http error ${searchResult.message()}")
                            }
                        },
                        { error ->
                            logE(null, error)
                        }
                )

    }

    private var tokenCounter = 0
    override fun refreshToken() {
        tokenCounter++
        model.refreshToken("token_$tokenCounter")
    }

}
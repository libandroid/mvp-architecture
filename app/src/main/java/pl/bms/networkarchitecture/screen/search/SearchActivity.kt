package pl.bms.networkarchitecture.screen.search

import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_search.*
import pl.bms.networkarchitecture.utils.visibleOrGone
import pl.bms.networkarchitecture.R
import pl.bms.networkarchitecture.base.BaseActivity


/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 05.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */


class SearchActivity : BaseActivity<SearchViewState, SearchContract.View, SearchContract.Presenter>(), SearchContract.View {

    override val statefulPresenter = true
    override val layoutId = R.layout.activity_search

    private val adapter: RepositoriesAdapter by lazy { RepositoriesAdapter() }

    override fun createPresenter(restoredViewState: SearchViewState?): SearchContract.Presenter {
        return SearchPresenter(SearchModel(app.restManager, app.apiProvider), restoredViewState)
    }

    override fun setupViews() {
        mainActivityRecyclerView.layoutManager = LinearLayoutManager(this)
        mainActivityRecyclerView.adapter = adapter
    }

    override fun initActions() {
        mainActivitySearchButton.setOnClickListener {
            presenter.search(mainActivitySearchEditText.text.toString())
        }
        mainActivityTokenButton.setOnClickListener { presenter.refreshToken() }
    }

    override fun render(newState: SearchViewState) {
        mainActivityProgressBar.visibleOrGone(newState.isLoading)
        if (currentState?.searchResult != newState.searchResult) {
            adapter.items = newState.searchResult?.items ?: emptyList()
        }
    }

}

package pl.bms.networkarchitecture.screen.dialogs

import kotlinx.android.synthetic.main.activity_dialogs.*
import pl.bms.dialogmanager.DialogManager
import pl.bms.dialogmanager.alertDialog
import pl.bms.dialogmanager.showDialog
import pl.bms.networkarchitecture.R
import pl.bms.networkarchitecture.base.BaseActivity

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 10.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */

class DialogsActivity : BaseActivity<DialogsViewState, DialogsContract.View, DialogsContract.Presenter>(), DialogsContract.View {
    override val statefulPresenter = true

    override val layoutId = R.layout.activity_dialogs
    override fun createPresenter(restoredViewState: DialogsViewState?): DialogsContract.Presenter {
        return DialogsPresenter(restoredViewState)
    }

    override fun setupViews() {
        dialogsActivityNumberPicker.minValue = 1
        dialogsActivityNumberPicker.maxValue = 10
    }

    override fun initActions() {
        dialogsActivityNumberPicker.setOnValueChangedListener { _, _, j -> presenter.setNumber(j) }
        dialogsActivityDismissPrevious.setOnClickListener { presenter.startDialogs(DialogManager.ShowOption.DISMISS_PREVIOUS) }
        dialogsActivityWaitInQueue.setOnClickListener { presenter.startDialogs(DialogManager.ShowOption.WAIT_ON_QUEUE) }
        dialogsActivityShowIfNoOthers.setOnClickListener { presenter.startDialogs(DialogManager.ShowOption.SHOW_IF_NO_OTHERS) }
        dialogsActivityShowOnTop.setOnClickListener { presenter.startDialogs(DialogManager.ShowOption.SHOW_ON_TOP) }
    }

    override fun render(newState: DialogsViewState) {
        dialogsActivityNumberPicker.value = newState.number
    }

    override fun showDialog(title: String, message: String, option: DialogManager.ShowOption) {
        showDialog(alertDialog {
            this.title = title
            this.message = message
        }, option, this)
    }

}

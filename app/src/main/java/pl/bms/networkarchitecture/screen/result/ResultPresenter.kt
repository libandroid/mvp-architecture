package pl.bms.networkarchitecture.screen.result

import pl.bms.dialogmanager.DialogManager
import pl.bms.mvparchitecture.mvp.BaseMvpPresenter

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 16.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */


class ResultPresenter(firstNumber: Int, secondNumber: Int, restoredViewState: ResultViewState?): BaseMvpPresenter<ResultViewState, ResultContract.View>(), ResultContract.Presenter {

    override var currentState: ResultViewState = restoredViewState ?: ResultViewState(firstNumber, secondNumber)

    override fun resultButtonPressed() {
        mvpView?.showDialog("Are you sure?",
                "you want to return the result of adding?",
                "Yes I'm",
                "No, I'm afraid",
                DialogManager.ShowOption.SHOW_ON_TOP)
    }

    override fun positiveAction() {
        mvpView?.finishWithResult(currentState.firstNumber + currentState.secondNumber)
    }

    override fun negativeAction() {
        mvpView?.showToast("Chicken!")
    }

}
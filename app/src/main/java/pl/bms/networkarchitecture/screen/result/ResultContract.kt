package pl.bms.networkarchitecture.screen.result

import pl.bms.dialogmanager.DialogManager
import pl.bms.mvparchitecture.mvp.MvpPresenter
import pl.bms.mvparchitecture.mvp.MvpView


/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 16.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */


interface ResultContract {

    interface View: MvpView<ResultViewState> {

        fun finishWithResult(sum: Int)

        fun showDialog(title: String, message: String, positiveButton: String, negativeButton: String, option: DialogManager.ShowOption)

        fun showToast(text: String)
    }

    interface Presenter: MvpPresenter<ResultViewState, ResultContract.View> {

        fun resultButtonPressed()

        fun positiveAction()

        fun negativeAction()

    }

}
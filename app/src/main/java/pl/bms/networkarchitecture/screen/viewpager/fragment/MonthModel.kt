package pl.bms.networkarchitecture.screen.viewpager.fragment

import pl.bms.network.ApiProvider
import io.reactivex.Single
import pl.bms.networkarchitecture.api.SearchApi
import pl.bms.networkarchitecture.model.RepoSearchResults
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 16.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */

class MonthModel(apiProvider: ApiProvider): MonthContract.Model {

    private val searchApi  = apiProvider.getApi(SearchApi::class.java)

    private var dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)

    private val cache = mutableMapOf<Pair<Date, Date>, Response<RepoSearchResults>>()

    override fun searchRepositoriesByDate(dateFrom: Date, dateTo: Date): Single<Response<RepoSearchResults>> {
        val cacheKey = dateFrom to dateTo
        if (cache.contains(cacheKey)) {
            return Single.just(cache[cacheKey])
        }

        val datesRangeString = "created:${dateFormat.format(dateFrom)}..${dateFormat.format(dateTo)}"
        return searchApi.searchRepositories(datesRangeString).doAfterSuccess{ response ->
            if (response.isSuccessful) {
                cache[cacheKey] = response
            }
        }
    }

}
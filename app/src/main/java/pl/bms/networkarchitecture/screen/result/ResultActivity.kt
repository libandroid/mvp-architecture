package pl.bms.networkarchitecture.screen.result

import android.app.Activity
import android.content.Intent
import kotlinx.android.synthetic.main.activity_result.*
import org.jetbrains.anko.toast
import pl.bms.dialogmanager.DialogEvent
import pl.bms.dialogmanager.DialogManager
import pl.bms.dialogmanager.alertDialog
import pl.bms.dialogmanager.showDialog
import pl.bms.networkarchitecture.R
import pl.bms.networkarchitecture.base.BaseActivity

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 12.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */


class ResultActivity: BaseActivity<ResultViewState, ResultContract.View, ResultContract.Presenter>(), ResultContract.View {


    override val statefulPresenter = false
    override val layoutId = R.layout.activity_result

    private val entry: ResultActivityEntry by lazy { ResultActivityEntry(intent) }

    override fun createPresenter(restoredViewState: ResultViewState?): ResultContract.Presenter {
        return ResultPresenter(entry.firstNumber, entry.secondNumber, restoredViewState)
    }

    override fun setupViews() {}

    override fun initActions() {
        resultActivityResultButton.setOnClickListener { presenter.resultButtonPressed() }
    }

    override fun showDialog(title: String, message: String, positiveButton: String, negativeButton: String, option: DialogManager.ShowOption) {
        showDialog(
                alertDialog {
                    this.message = message
                    this.title = title
                    this.positiveButton = positiveButton
                    this.negativeButton = negativeButton
                }
        )
    }

    override fun render(newState: ResultViewState) {
        resultActivityFirstNumberTv.text = "${newState.firstNumber}"
        resultActivitySecondNumberTv.text = "${newState.secondNumber}"
    }

    override fun finishWithResult(sum: Int) {
        setResult(Activity.RESULT_OK, Intent().apply { putExtra(ResultActivity.RESULT_SUM_OF_NUMBERS, sum) })
        finish()
    }

    companion object {
        const val RESULT_SUM_OF_NUMBERS = "result.sum.of.numbers"
    }

    override fun handleDialogEvent(dialogEvent: DialogEvent) {
        when(dialogEvent) {
            is DialogEvent.Positive -> {
                presenter.positiveAction()
            }
            is DialogEvent.Negative -> {
                presenter.negativeAction()
            }
        }
    }

    override fun showToast(text: String) {
        toast(text)
    }
}

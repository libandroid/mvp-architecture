package pl.bms.networkarchitecture.screen.viewpager

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import kotlinx.android.synthetic.main.activity_viewpager.*
import pl.bms.mvparchitecture.mvp.EmptyViewState
import pl.bms.networkarchitecture.R
import pl.bms.networkarchitecture.base.BaseActivity
import pl.bms.networkarchitecture.screen.viewpager.fragment.MonthFragment
import pl.bms.networkarchitecture.screen.viewpager.fragment.MonthModel

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 16.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */

class ViewPagerActivity: BaseActivity<EmptyViewState, ViewPagerContract.View, ViewPagerContract.Presenter>(),ViewPagerContract.View {

    override val layoutId = R.layout.activity_viewpager
    val monthModel by lazy { MonthModel(app.apiProvider) }

    override fun createPresenter(restoredViewState: EmptyViewState?):ViewPagerContract.Presenter {
        return ViewPagerPresenter()
    }

    override fun setupViews() {
        viewpagerActivityViewPager.adapter = MonthPagerAdapter(supportFragmentManager)
        viewpagerActivityViewPager.isHorizontalScrollBarEnabled = false
        viewpagerActivityTabLayout.setUpWithViewPager(viewpagerActivityViewPager)
        viewpagerActivityViewPager.currentItem = presenter.maxMonths - 1
    }

    override fun initActions() {}

    override fun render(newState: EmptyViewState) {}

    inner class MonthPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            val fromAndToDates = presenter.getMonthsFromToByPosition(position)
            return MonthFragment.newInstance(fromAndToDates.first, fromAndToDates.second)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return presenter.getMonthTitleByPosition(position)
        }

        override fun getCount(): Int {
            return presenter.maxMonths
        }

    }

}
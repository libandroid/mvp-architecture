package pl.bms.networkarchitecture.screen.start

import android.app.Activity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_start.*
import org.jetbrains.anko.toast
import pl.bms.navigation.NavigatorActivity
import pl.bms.networkarchitecture.R
import pl.bms.networkarchitecture.screen.dialogs.DialogsActivityEntry
import pl.bms.networkarchitecture.screen.result.ResultActivity
import pl.bms.networkarchitecture.screen.result.ResultActivityEntry
import pl.bms.networkarchitecture.screen.search.SearchActivityEntry
import pl.bms.networkarchitecture.screen.viewpager.ViewPagerActivityEntry

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 12.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */

class StartActivity : NavigatorActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        startActivityToSearchActivityButton.setOnClickListener {
            navigate(SearchActivityEntry())
        }

        startActivityToResultActivityButton.setOnClickListener {
            navigate(ResultActivityEntry(223, 343)) { resultCode, data ->
                this is StartActivity
                val sum = data?.getInt(ResultActivity.RESULT_SUM_OF_NUMBERS)
                if (resultCode == Activity.RESULT_OK) {
                    toast("sum of numbers = $sum")
                } else {
                    toast("result canceled")
                }
            }
        }

        startActivityToViewpagerActivityButton.setOnClickListener {
            navigate(ViewPagerActivityEntry())
        }

        startActivityToDialogsActivityButton.setOnClickListener {
            navigate(DialogsActivityEntry())
        }

    }

}
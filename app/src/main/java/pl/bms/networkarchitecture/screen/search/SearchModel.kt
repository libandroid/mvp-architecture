package pl.bms.networkarchitecture.screen.search

import pl.bms.network.ApiProvider
import pl.bms.network.RestManager
import io.reactivex.Single
import pl.bms.networkarchitecture.api.SearchApi
import pl.bms.networkarchitecture.model.RepoSearchResults
import retrofit2.Response

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 11.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */


class SearchModel(private val restManager: RestManager, apiProvider: ApiProvider): SearchContract.Model {

    private val searchApi = apiProvider.getApi(SearchApi::class.java)

    override fun search(searchString: String): Single<Response<RepoSearchResults>> {
        return searchApi.searchRepositories(searchString)
    }

    override fun refreshToken(token: String) {
        restManager.refreshToken(token)
    }

}
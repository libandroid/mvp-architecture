package pl.bms.networkarchitecture.screen.viewpager

import pl.bms.mvparchitecture.mvp.BaseMvpPresenter
import pl.bms.mvparchitecture.mvp.EmptyViewState
import org.joda.time.DateTime
import pl.bms.networkarchitecture.R
import java.util.*

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 17.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */


class ViewPagerPresenter: BaseMvpPresenter<EmptyViewState, ViewPagerContract.View>(), ViewPagerContract.Presenter {

    override var currentState = EmptyViewState()
    override fun onCreate() {}
    override fun onDestroy() {}

    override val maxMonths = 1000
    private val lastPosition = maxMonths - 1

    override fun getMonthsFromToByPosition(position: Int): Pair<Date, Date> {
        return DateTime().minusMonths(lastPosition - position).let { month ->
            val from = month.withDayOfMonth(1).withTimeAtStartOfDay().toDate()
            val to = month.dayOfMonth().withMaximumValue().toDate()
            from to to
        }
    }

    override fun getMonthTitleByPosition(position: Int): String {
        return DateTime().minusMonths(lastPosition - position).toString(context?.getString(R.string.tab_date_format))
    }

}
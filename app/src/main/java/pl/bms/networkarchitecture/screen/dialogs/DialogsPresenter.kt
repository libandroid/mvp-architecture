package pl.bms.networkarchitecture.screen.dialogs

import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import pl.bms.dialogmanager.DialogManager
import pl.bms.mvparchitecture.mvp.BaseMvpPresenter
import java.util.concurrent.TimeUnit

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 10.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */

class DialogsPresenter(restoredViewState: DialogsViewState?) : BaseMvpPresenter<DialogsViewState, DialogsContract.View>(), DialogsContract.Presenter {

    override var currentState: DialogsViewState = restoredViewState ?: DialogsViewState()

    private var disposable: Disposable? = null

    override fun setNumber(number: Int) {
        currentState = DialogsViewState(number)
    }

    override fun startDialogs(type: DialogManager.ShowOption) {
        disposable?.dispose()
        disposable = Flowable.intervalRange(1, currentState.number.toLong(), 0L, 300L, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { number ->
                    mvpView?.showDialog("Dialog $type", "number $number", type)
                }

    }

    override fun onDestroy() {
        disposable?.dispose()
        disposable = null
        super.onDestroy()
    }

}

package pl.bms.networkarchitecture.screen.viewpager.fragment

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_month.*
import pl.bms.networkarchitecture.utils.visibleOrGone
import kotlinx.android.synthetic.main.fragment_month.view.*
import pl.bms.networkarchitecture.base.BaseFragment
import pl.bms.networkarchitecture.screen.viewpager.ViewPagerActivity
import pl.bms.networkarchitecture.screen.search.RepositoriesAdapter
import java.util.*

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 16.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */

class MonthFragment: BaseFragment<MonthViewState, MonthContract.View, MonthContract.Presenter>(), MonthContract.View {

    override val layoutId = pl.bms.networkarchitecture.R.layout.fragment_month

    private val from: Date? by lazy { arguments?.getSerializable(MonthFragment.PARAM_FROM) as Date? }
    private val to: Date? by lazy { arguments?.getSerializable(MonthFragment.PARAM_TO) as Date? }
    private val adapter = RepositoriesAdapter()

    override fun setupViews(view: View) {
        view.monthFragmentList.layoutManager = LinearLayoutManager(context)
        view.monthFragmentList.adapter = adapter
    }

    override fun initActions(view: View) {}

    override fun render(newState: MonthViewState) {
        monthFragmentProgressBar.visibleOrGone(newState.isLoading)
        monthFragmentErrorTv.text = newState.errorMessage
        if (newState.searchResult != currentState?.searchResult) {
            adapter.items = newState.searchResult?.items ?: emptyList()
        }
    }

    override fun createPresenter(restoredViewState: MonthViewState?): MonthContract.Presenter {
        return MonthPresenter((activity as ViewPagerActivity).monthModel, from ?: Date(), to ?: Date(), restoredViewState)
    }

    companion object {
        const val PARAM_FROM = "month.param.from"
        const val PARAM_TO = "month.param.to"

        fun newInstance(from: Date, to: Date) : MonthFragment {
            return MonthFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(MonthFragment.PARAM_FROM, from)
                    putSerializable(MonthFragment.PARAM_TO, to)
                }
            }
        }
    }


}
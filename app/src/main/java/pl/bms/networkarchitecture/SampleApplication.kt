package pl.bms.networkarchitecture

import android.app.Application
import net.danlew.android.joda.JodaTimeAndroid
import pl.bms.network.ApiProvider
import pl.bms.network.RestBuilder
import pl.bms.network.error.HttpError
import pl.bms.network.error.NetworkError
import pl.bms.networkarchitecture.serializer.DateSerializer
import java.util.*

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 10.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */

class SampleApplication: Application() {

    companion object {
        const val BASE_URL = "https://api.github.com"
    }

    override fun onCreate() {
        super.onCreate()
        JodaTimeAndroid.init(this)
    }

    val restManager by lazy {
        RestBuilder()
                .setBaseUrl(SampleApplication.Companion.BASE_URL)
                .addGsonTypeAdapter(Date::class.java, DateSerializer())
                .setLoggingLevel(RestBuilder.LoggingLevel.HEADERS)
                .handleHttpErrors { httpError ->
                    httpErrorHandler?.invoke(httpError)
                }
                .handleNetworkErrors { error ->
                    networkErrorHandler?.invoke(error)
                }
                .build()
    }

    val apiProvider by lazy {
        restManager.apiProvider
    }

    var httpErrorHandler: ((HttpError) -> Unit)? = null
    var networkErrorHandler: ((NetworkError) -> Unit)? = null

}
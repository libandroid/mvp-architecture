package pl.bms.networkarchitecture.base

import pl.bms.mvparchitecture.mvp.BaseMvpFragment
import pl.bms.mvparchitecture.mvp.MvpPresenter
import pl.bms.mvparchitecture.mvp.MvpView
import pl.bms.mvparchitecture.mvp.MvpViewState
import pl.bms.networkarchitecture.SampleApplication

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 17.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */
abstract class BaseFragment<S: MvpViewState, V : MvpView<S>, P : MvpPresenter<S, V>>: BaseMvpFragment<S, V, P>() {

    val app by lazy { activity?.application as SampleApplication }

}
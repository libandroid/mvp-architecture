package pl.bms.networkarchitecture.base

import pl.bms.dialogmanager.*
import pl.bms.mvparchitecture.mvp.BaseMvpActivity
import pl.bms.mvparchitecture.mvp.MvpPresenter
import pl.bms.mvparchitecture.mvp.MvpView
import pl.bms.mvparchitecture.mvp.MvpViewState
import pl.bms.network.error.HttpError
import pl.bms.network.error.NetworkError
import pl.bms.networkarchitecture.SampleApplication
import pl.bms.networkarchitecture.utils.logD
import java.util.*

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 10.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */
abstract class BaseActivity<S: MvpViewState, V : MvpView<S>, P : MvpPresenter<S, V>>: BaseMvpActivity<S, V, P>(), DialogFragmentProvider {

    val app: SampleApplication by lazy { application as SampleApplication }

    override fun onResume() {
        super.onResume()
        DialogManager.registerActivity(this)
        app.httpErrorHandler = httpErrorHandler
        app.networkErrorHandler = networkErrorHandler
    }

    override fun onPause() {
        DialogManager.unregisterActivity(this)
        app.httpErrorHandler = null
        app.networkErrorHandler = null
        super.onPause()
    }

    override fun handleDialogEvent(dialogEvent: DialogEvent) {}

    private var httpErrorHandler: ((HttpError) -> Unit) = { error ->
        val random = Random().nextInt(1000)
        logD( "error handler random: $random")
        showDialog(alertDialog {
            title = "Http error (Random: $random)"
            message = "Handle http error ${error.message}"
        }, DialogManager.ShowOption.SHOW_IF_NO_OTHERS)
    }

    private var networkErrorHandler: ((NetworkError) -> Unit) = { error ->
        val random = Random().nextInt(1000)
        logD("error handler random: $random")
        showDialog(alertDialog {
            title = "Network error (Random: $random)"
            message = "Handle network error: ${error.throwable.message}"
        }, DialogManager.ShowOption.SHOW_IF_NO_OTHERS)
    }

}
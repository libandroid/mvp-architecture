package pl.bms.networkarchitecture

import io.reactivex.Single
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner
import pl.bms.networkarchitecture.model.GitHubRepo
import pl.bms.networkarchitecture.model.RepoSearchResults
import pl.bms.networkarchitecture.model.User
import pl.bms.networkarchitecture.screen.search.SearchContract
import pl.bms.networkarchitecture.screen.search.SearchPresenter
import pl.bms.networkarchitecture.screen.search.SearchViewState
import retrofit2.Response
import java.util.*

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 15.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */


@RunWith(MockitoJUnitRunner::class)
class SearchPresenterUnitTest {

    private lateinit var presenter: SearchPresenter

    @Mock
    private lateinit var view: SearchContract.View

    @Mock
    private lateinit var model: SearchContract.Model

    @Before
    fun setup() {
        presenter = SearchPresenter(model, null)
        presenter.onCreate()
        presenter.attachView(view)
    }

    @After
    fun finalize() {
        presenter.detachView()
        presenter.onDestroy()
    }

    @Test
    fun searchTest() {
        //Configure
        val testSearch = "testSearch"
        val testResult = RepoSearchResults(1, listOf(GitHubRepo(1, "testRepo", Date(), User("testOwner", null))))
        `when`(model.search(testSearch)).thenReturn(Single.just(Response.success(testResult)))

        //Actions
        presenter.search(testSearch)

        //Verify
        verify(view).refreshState(SearchViewState(true, null))
        verify(view).refreshState(SearchViewState(false, testResult))

    }

}

package pl.bms.networkarchitecture

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import pl.bms.networkarchitecture.model.GitHubRepo
import pl.bms.networkarchitecture.model.RepoSearchResults
import pl.bms.networkarchitecture.model.User
import pl.bms.networkarchitecture.screen.search.SearchContract
import pl.bms.networkarchitecture.screen.search.SearchPresenter
import pl.bms.networkarchitecture.screen.search.SearchViewState
import java.util.*

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 15.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */

@RunWith(MockitoJUnitRunner::class)
class MvpPresenterUnitTest {

    private lateinit var presenter: SearchPresenter

    @Mock
    private lateinit var view: SearchContract.View

    @Mock
    private lateinit var model: SearchContract.Model

    @Before
    fun setup() {
        presenter = SearchPresenter(model, null)
        presenter.onCreate()
        presenter.attachView(view)
    }

    @After
    fun finalize() {
        presenter.detachView()
        presenter.onDestroy()
    }


    @Test
    fun refreshStateAfterAttachTest() {
        //Configure
        val defaultViewState = SearchViewState(false, null)

        //Actions
        presenter.detachView()
        presenter.attachView(view) //second attach (first is called in setup() )

        //Verify
        Mockito.verify(view, Mockito.times(2)).refreshState(defaultViewState)
    }

    @Test
    fun notRefreshingEqualsStatesTest() {
        //Configure
        val testRepoResult = RepoSearchResults(1, listOf(GitHubRepo(1, "testRepo", Date(), User("testOwner", null))))
        val testViewState = SearchViewState(false, testRepoResult)

        //Actions
        presenter.refreshState(testViewState)
        presenter.refreshState(testViewState)

        //Verify
        Mockito.verify(view, Mockito.times(1)).refreshState(testViewState)
    }


}
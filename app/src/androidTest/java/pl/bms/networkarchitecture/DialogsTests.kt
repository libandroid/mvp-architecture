package pl.bms.networkarchitecture

import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.doesNotExist
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withText
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import org.hamcrest.CoreMatchers.startsWith
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import pl.bms.dialogmanager.DialogManager
import pl.bms.networkarchitecture.screen.dialogs.DialogsActivity

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 22.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */
@RunWith(AndroidJUnit4::class)
class DialogsTests {

    @get:Rule
    private var activityRule = ActivityTestRule<DialogsActivity>(DialogsActivity::class.java)

    @Test
    fun dialogDismissPreviousTest() {

        activityRule.activity.presenter.setNumber(2)
        activityRule.activity.presenter.startDialogs(DialogManager.ShowOption.DISMISS_PREVIOUS)
        Thread.sleep(2000)

        onView(withText("Dialog ${DialogManager.ShowOption.DISMISS_PREVIOUS}")).check(matches(isDisplayed()))
        onView(withText("number 2")).check(matches(isDisplayed()))
        onView(withText("number 1")).check(doesNotExist())

        Espresso.pressBack()
        Thread.sleep(1000)
        onView(withText(startsWith("Dialog"))).check(doesNotExist())

    }

    @Test
    fun dialogWaitInQueueTest() {

        activityRule.activity.presenter.setNumber(2)
        activityRule.activity.presenter.startDialogs(DialogManager.ShowOption.WAIT_ON_QUEUE)
        Thread.sleep(2000)

        onView(withText("Dialog ${DialogManager.ShowOption.WAIT_ON_QUEUE}")).check(matches(isDisplayed()))
        onView(withText("number 1")).check(matches(isDisplayed()))
        onView(withText("number 2")).check(doesNotExist())

        Espresso.pressBack()
        Thread.sleep(2000)

        onView(withText("number 2")).check(matches(isDisplayed()))
        onView(withText("number 1")).check(doesNotExist())

        Espresso.pressBack()
        Thread.sleep(2000)

        onView(withText(startsWith("Dialog"))).check(doesNotExist())

    }

    @Test
    fun dialogShowIfNoOthersTest() {

        activityRule.activity.presenter.setNumber(2)
        activityRule.activity.presenter.startDialogs(DialogManager.ShowOption.SHOW_IF_NO_OTHERS)
        Thread.sleep(2000)

        onView(withText("Dialog ${DialogManager.ShowOption.SHOW_IF_NO_OTHERS}")).check(matches(isDisplayed()))
        onView(withText("number 1")).check(matches(isDisplayed()))
        onView(withText("number 2")).check(doesNotExist())

        Espresso.pressBack()
        Thread.sleep(1000)

        onView(withText(startsWith("Dialog"))).check(doesNotExist())

    }

    @Test
    fun dialogShowOnTopTest() {

        activityRule.activity.presenter.setNumber(2)
        activityRule.activity.presenter.startDialogs(DialogManager.ShowOption.SHOW_ON_TOP)
        Thread.sleep(2000)

        onView(withText("Dialog ${DialogManager.ShowOption.SHOW_ON_TOP}")).check(matches(isDisplayed()))
        onView(withText("number 2")).check(matches(isDisplayed()))

        Espresso.pressBack()
        onView(withText("number 1")).check(matches(isDisplayed()))

    }

    @Test
    fun removeQueueTest() {

        activityRule.activity.showDialog("Queue1", "dialog", DialogManager.ShowOption.WAIT_ON_QUEUE)
        activityRule.activity.showDialog("Queue2", "dialog", DialogManager.ShowOption.WAIT_ON_QUEUE)
        activityRule.activity.showDialog("Queue3", "dialog", DialogManager.ShowOption.WAIT_ON_QUEUE)
        activityRule.activity.showDialog("Queue4", "dialog", DialogManager.ShowOption.WAIT_ON_QUEUE)
        activityRule.activity.showDialog("DISMISS_PREVIOUS", "dialog", DialogManager.ShowOption.DISMISS_PREVIOUS)
        Thread.sleep(2000)

        onView(withText("DISMISS_PREVIOUS")).check(matches(isDisplayed()))
        Espresso.pressBack()
        Thread.sleep(1000)

        onView(withText(startsWith("dialog"))).check(doesNotExist())
    }

    @Test
    fun removeShowedOnTop() {
        activityRule.activity.showDialog("Top1", "dialog", DialogManager.ShowOption.SHOW_ON_TOP)
        activityRule.activity.showDialog("Top2", "dialog", DialogManager.ShowOption.SHOW_ON_TOP)
        activityRule.activity.showDialog("DISMISS_PREVIOUS", "dialog", DialogManager.ShowOption.DISMISS_PREVIOUS)
        Thread.sleep(4000)

        onView(withText("DISMISS_PREVIOUS")).check(matches(isDisplayed()))
        Espresso.pressBack()
        Thread.sleep(1000)

        onView(withText(startsWith("dialog"))).check(doesNotExist())

    }

}
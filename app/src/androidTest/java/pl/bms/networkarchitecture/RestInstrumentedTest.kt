package pl.bms.networkarchitecture

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import android.util.Log
import pl.bms.network.RestBuilder
import pl.bms.network.RestManager
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.BufferedInputStream
import java.net.UnknownHostException
import java.security.cert.CertificateFactory
import javax.net.ssl.SSLHandshakeException

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 12.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */

@RunWith(AndroidJUnit4::class)
class RestInstrumentedTest {

    private lateinit var builder: RestBuilder
    private lateinit var mockWebServer: MockWebServer
    private val serverWithCertificate = "https://192.168.0.110:8443/"

    @Before
    fun setup() {
        builder = RestBuilder().apply {
            setBaseUrl("http://example.com")
            setSubscribeScheduler(Schedulers.trampoline())
            setObserveScheduler(Schedulers.trampoline())
        }
        mockWebServer = MockWebServer().apply {
            start()
        }
    }

    @Test
    fun testHandleHttpError() {
        var handled = false

        val manager = builder
                .handleHttpErrors {
                    handled = true
                }
                .build()

        makeTestCall(manager)
        assertTrue(!handled)

        makeCallWithHttpError(manager)
        Thread.sleep(10) //wait because handle error run on main thread
        assertTrue(handled)

        mockWebServer.shutdown()
    }


    @Test
    fun testNetworkHttpError() {
        var handled = false

        val manager = builder
                .handleNetworkErrors {
                    handled = true
                }
                .build()

        makeTestCall(manager)
        assertTrue(!handled)

        try {
            makeCallWithNetworkError(manager)
        } catch (e: UnknownHostException) {
            Log.e("RestInstrumentedTest", "error cached: ${e.message}")
        }
        Thread.sleep(10) //wait because handle error run on main thread
        assertTrue(handled)

        mockWebServer.shutdown()
    }

    private fun makeTestCall(manager: RestManager): RecordedRequest {
        mockWebServer.enqueue(MockResponse())
        manager.newCall(Request.Builder().url(mockWebServer.url("/")).build()).execute()
        return mockWebServer.takeRequest()
    }

    private fun makeCallWithNetworkError(manager: RestManager) {
        manager.newCall(Request.Builder().url("http://bad-not-existing---url.pl").build()).execute()
    }

    private fun makeCallWithHttpError(manager: RestManager) {
        val response = MockResponse()
        response.setResponseCode(404)
        mockWebServer.enqueue(response)
        manager.newCall(Request.Builder().url(mockWebServer.url("/")).build()).execute()
    }

    /**
     * Works only on server configured with certificate /app/src/androidTest/assets/server.ctr
     */
    @Test
    fun sslTestCustomCertificate() {
        val context = InstrumentationRegistry.getContext()

        val cf = CertificateFactory.getInstance("X.509")
        // From https://www.washington.edu/itconnect/security/ca/load-der.crt
        val inputStream = context.resources.assets.open("server.crt")
        val caInput = BufferedInputStream(inputStream)
        val ca = cf.generateCertificate(caInput)

        val manager = builder
                .setCertificate(ca)
                .build()

        manager.newCall(Request.Builder().url(serverWithCertificate).get().build()).execute()

    }

    /**
     * Works only on server configured with untrusted certificate
     */
    @Test
    fun sslTestTrustAll() {
        val manager = builder
                .trustAllCertificates(true)
                .build()
        manager.newCall(Request.Builder().url(serverWithCertificate).get().build()).execute()
    }

    /**
     * Works only on server configured with untrusted certificate
     */
    @Test(expected = SSLHandshakeException::class)
    fun sslTestFail() {
        val manager = builder
                .build()
        manager.newCall(Request.Builder().url(serverWithCertificate).get().build()).execute()
    }

}

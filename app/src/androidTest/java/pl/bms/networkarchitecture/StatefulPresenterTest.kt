package pl.bms.networkarchitecture

import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import pl.bms.networkarchitecture.utils.TestUtils.Companion.getActivityInstance
import pl.bms.networkarchitecture.utils.TestUtils.Companion.rotateScreen
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import pl.bms.networkarchitecture.screen.search.SearchActivity
import pl.bms.networkarchitecture.screen.search.SearchPresenter

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 15.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */

@RunWith(AndroidJUnit4::class)
class StatefulPresenterTest {

    @get:Rule
    private var activityRule = ActivityTestRule<SearchActivity>(SearchActivity::class.java)

    @Test
    fun statefulPresenterTest() {
        val startingPresenter = activityRule.activity.presenter as SearchPresenter

        rotateScreen(activityRule)
        Thread.sleep(2000)

        rotateScreen(activityRule)
        Thread.sleep(2000)

        check(startingPresenter === getActivityInstance<SearchActivity>()?.presenter)
    }

}
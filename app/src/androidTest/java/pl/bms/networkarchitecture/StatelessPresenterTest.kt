package pl.bms.networkarchitecture

import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import pl.bms.networkarchitecture.screen.result.ResultActivity
import pl.bms.networkarchitecture.screen.result.ResultPresenter
import pl.bms.networkarchitecture.utils.TestUtils

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 16.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */

@RunWith(AndroidJUnit4::class)
class StatelessPresenterTest {

    @get:Rule
    private val activityRule = ActivityTestRule<ResultActivity>(ResultActivity::class.java)

    @Test
    fun statelessPresenterTest() {
        val startingPresenter = activityRule.activity.presenter as ResultPresenter

        pl.bms.networkarchitecture.utils.TestUtils.rotateScreen(activityRule)
        Thread.sleep(2000)

        pl.bms.networkarchitecture.utils.TestUtils.rotateScreen(activityRule)
        Thread.sleep(2000)

        check(startingPresenter !== TestUtils.getActivityInstance<ResultActivity>()?.presenter)
    }

}
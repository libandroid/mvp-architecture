package pl.bms.mvparchitecture.mvp

import android.content.Context
import android.os.Bundle
import pl.bms.navigation.NavigatorActivity

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 10.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */

abstract class BaseMvpActivity<S: MvpViewState, V : MvpView<S>, P : MvpPresenter<S, V>> : NavigatorActivity(), MvpView<S> {

    /**  if statefulPresenter is true Presenter retains after orientation change */
    open val statefulPresenter = false
    open val doNotSaveInstanceState = false

    lateinit var presenter: P

    /**  currentState is updated after render(newState: S) finish */
    var currentState: S? = null

    abstract val layoutId: Int

    override val mvpContext: Context?
        get() = this

    /**
     * Create new Presenter instance. Is called in onCreate(), before setContentView(layoutId)
     * If statefulPresenter is true, and there is saved Presenter instance, this method will not be called
     */
    abstract fun createPresenter(restoredViewState: S?): P

    /**
     * place to configure all views, setup adapters, e.t.c
     * called in onCreate() after presenter's initialization, and before initActions()
     */
    open fun setupViews() {}

    /**
     * init all views actions, listeners
     * called in onCreate() after presenter's initialization, and after setupViews()
     */
    open fun initActions() {}

    /**
     * render new refreshed state.
     * currentState is set to new after finish this function
     */
    abstract fun render(newState: S)


    private fun initPresenter(bundle: Bundle?) {
        val restoredViewState: S? = bundle?.let {
            if (bundle.containsKey(SAVED_VIEWSTATE)) bundle.getParcelable(SAVED_VIEWSTATE) else null
        }
        presenter = if (statefulPresenter) {
            (lastCustomNonConfigurationInstance as? P) ?: createPresenter(restoredViewState).apply { onCreate() }
        } else {
            createPresenter(restoredViewState).apply { onCreate() }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId)
        initPresenter(savedInstanceState)
        setupViews()
        initActions()
    }

    final override fun onRetainCustomNonConfigurationInstance(): Any {
        return presenter
    }

    override fun onResume() {
        super.onResume()
        @Suppress("UNCHECKED_CAST")
        presenter.attachView(this as V)
    }

    override fun onPause() {
        presenter.detachView()
        super.onPause()
    }

    override fun onDestroy() {
        if (isFinishing || !statefulPresenter) {
            presenter.onDestroy()
        }
        super.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        if (!doNotSaveInstanceState) {
            currentState?.also { state -> outState?.putParcelable(SAVED_VIEWSTATE, state) }
        }
        super.onSaveInstanceState(outState)
    }

    final override fun refreshState(viewState: S) {
        render(viewState)
        currentState = viewState
    }

    companion object {
        private val SAVED_VIEWSTATE = "saved.viewstate.${this::class.java.simpleName}"
    }

}
package pl.bms.mvparchitecture.mvp

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import pl.bms.navigation.NavigatorFragment

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 10.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */
abstract class BaseMvpFragment<S: MvpViewState, V: MvpView<S>, P: MvpPresenter<S, V>>: NavigatorFragment(), MvpView<S> {

    lateinit var presenter: P
    var currentState: S? = null
    open val doNotSaveInstanceState = false

    abstract val layoutId: Int

    override val mvpContext: Context?
        get() = context

    /**
     * place to configure all views, setup adapters, e.t.c
     * called in onCreateView() after presenter's initialization, and before initActions()
     */
    open fun setupViews(view: View) {}

    /**
     * init all views actions, listeners
     * called in onCreateView() after presenter's initialization, and after setupViews()
     */
    open fun initActions(view: View) {}

    /**
     * render new refreshed state.
     * currentState is set to new one after finish this function
     */
    abstract fun render(newState: S)
    /**
     * Create new Presenter instance. Is called in onCreate(), before inflating view
     */
    abstract fun createPresenter(restoredViewState: S?) : P

    private fun initPresenter(bundle: Bundle?) {
        val restoredViewState: S? = bundle?.let {
            if (bundle.containsKey(SAVED_VIEWSTATE)) bundle.getParcelable(SAVED_VIEWSTATE) else null
        }
        presenter = createPresenter(restoredViewState).apply { onCreate() }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutId, container, false).also { view ->
            initPresenter(savedInstanceState)
            setupViews(view)
            initActions(view)
        }
    }

    override fun onResume() {
        super.onResume()
        @Suppress("UNCHECKED_CAST")
        presenter.attachView(this as V)
    }

    override fun onPause() {
        presenter.detachView()
        super.onPause()
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    final override fun refreshState(viewState: S) {
        render(viewState)
        currentState = viewState
    }

    override fun onSaveInstanceState(outState: Bundle) {
        if (!doNotSaveInstanceState) {
            currentState?.also { state -> outState.putParcelable(SAVED_VIEWSTATE, state) }
        }
        super.onSaveInstanceState(outState)
    }

    companion object {
        private val SAVED_VIEWSTATE = "saved.viewstate.${this::class.java.simpleName}"
    }
}
package pl.bms.mvparchitecture.mvp

import android.content.Context

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 10.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */
abstract class BaseMvpPresenter<S: MvpViewState, V: MvpView<S>> : MvpPresenter<S, V> {

    var mvpView: V? = null

    abstract var currentState: S

    override fun onCreate() {}

    override val context: Context?
        get() = mvpView?.mvpContext

    /**
     * method called on Activity/Fragment onResume(), after attaching mvpView, and after refreshing mvpView's viewState.
     */
    open fun onViewAttached() {}

    /**
     * method called on Activity/Fragment onPause(), just before detaching mvpView.
     */
    open fun viewWillBeDetached() {}

    override fun onDestroy() {}


    final override fun attachView(view: V) {
        mvpView = view
        mvpView?.refreshState(currentState)
        onViewAttached()
    }

    final override fun detachView() {
        viewWillBeDetached()
        mvpView = null
    }

    /**
     * use this method to refreshing mvpView's viewState instead of mvpView.refreshState()
     */
    fun refreshState(newState: S) {
        if (currentState != newState) {
            currentState = newState
            mvpView?.refreshState(currentState)
        }
    }

}
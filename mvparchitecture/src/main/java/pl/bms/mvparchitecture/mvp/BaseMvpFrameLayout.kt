@file:Suppress("unused")

package pl.bms.mvparchitecture.mvp

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import android.util.AttributeSet
import android.widget.FrameLayout
import java.io.Serializable

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 02.02.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */
@Suppress("LeakingThis")
abstract class BaseMvpFrameLayout<S: MvpViewState, V: MvpView<S>, P: MvpPresenter<S, V>>: FrameLayout,  MvpView<S>  {

    lateinit var presenter: P
    var currentState: S? = null
    open val doNotSaveInstanceState = false

    override val mvpContext: Context?
        get() = context

    /**
     * Using Serializable to save and restore instance state instead of faster Parcelable
     */
    abstract fun createPresenter(restoredViewState: S?) : P

    abstract fun initView(context: Context, attrs: AttributeSet?, defStyleAttr: Int?)

    /**
     * init all views actions, listeners
     * called in constructor after initView()
     */
    open fun initActions() {}

    abstract fun render(newState: S)

    constructor(context: Context) : super(context) {
        initView(context, null, null)
        initActions()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs){
        initView(context, attrs, null)
        initActions()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr){
        initView(context, attrs, defStyleAttr)
        initActions()
    }

    override fun onSaveInstanceState(): Parcelable? {
        if (!doNotSaveInstanceState) {
            return super.onSaveInstanceState().let {
                SavedState(it).apply {
                    viewState = currentState
                }
            }
        }
        return super.onSaveInstanceState()
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        val savedState = state as? SavedState
        super.onRestoreInstanceState(savedState?.superState)
        presenter = createPresenter(savedState?.viewState as? S).apply { onCreate() }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        if (!::presenter.isInitialized) {
            presenter = createPresenter(null).apply { onCreate() }
        }
        @Suppress("UNCHECKED_CAST")
        presenter.attachView(this as V)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        presenter.detachView()
        presenter.onDestroy()
    }

    final override fun refreshState(viewState: S) {
        render(viewState)
        currentState = viewState
    }

    internal class SavedState : BaseSavedState {

        var viewState: Serializable? = null

        constructor(superState: Parcelable) : super(superState)

        private constructor(source: Parcel) : super(source) {
            viewState = source.readSerializable()
        }

        override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
            super.writeToParcel(this, flags)
            writeSerializable(viewState)
        }

        companion object {
            @JvmField
            val CREATOR: Parcelable.Creator<SavedState> = object : Parcelable.Creator<SavedState> {
                override fun createFromParcel(source: Parcel): SavedState = SavedState(source)
                override fun newArray(size: Int): Array<SavedState?> = arrayOfNulls(size)
            }
        }
    }



}